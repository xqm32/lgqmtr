from solve import post
from settings import *
import argparse

parser = argparse.ArgumentParser(description='临高启明论坛爬虫')
parser.parse_args()

f = None
if get_setting('日志文件') != '':
    f = open(get_setting('日志文件'), 'w', encoding='utf-8_sig')
    set_log(f.write)

arg = input().split()
if len(arg) == 1:
    post(arg[0])
elif len(arg) == 2:
    post(arg[0], int(arg[1]))
elif len(arg) == 3:
    post(arg[0], int(arg[1]), int(arg[2]))
elif len(arg) == 4:
    post(arg[0], int(arg[1]), int(arg[2]), arg[3])

if f:
    f.close()
