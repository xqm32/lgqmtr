import os
import requests
from status import *
from get import *
from settings import *


def set_quote(state=True):
    Status.is_quote = state
    return str()


def set_pass(status=False):
    Status.is_pass = status
    return str()


def text(t):  # 第一个子标签前的内容，参见 https://lxml.de/api/lxml.etree._Element-class.html 中的 text
    return t.text.strip() if t.text else str()


def tail(t):  # 标签结尾的内容，参见 https://lxml.de/api/lxml.etree._Element-class.html 中的 tail
    return t.tail.strip() if t.tail else str()


def after(t):
    # 没有加入正在处理的，因而需要将后面的字符串加入
    content = tail(t)
    for i in t.itersiblings():
        content += tag(i)
    return content


def img(t):
    if not os.path.exists(f'img/{Status.tid}'):
        os.makedirs(f'img/{Status.tid}')
    url = t.get('file')
    fn = re.compile(
        r'([^/]+\.(zip|rar|gif|jpg|jpeg|png|svg))').search(url).group(1)
    # 后缀名不能超过四个字符，否则会抛异常
    if 'https://' not in url and 'http://' not in url:
        url = 'https://lgqm.gq/'+url
    if not os.path.exists(f'img/{Status.tid}/{fn}'):
        with open(f'img/{Status.tid}/{fn}', 'wb') as f:
            f.write(get_img(f"{url}"))
    return f'\n[[image:{fn}{get_setting("图片设置")}]]\n'


i_class = {  # i 标签的 class 判断函数
    'pstatus': lambda t: tail(t)  # 发表或者编辑的时间
}

div_class = {  # div 标签的 class 的判断函数
    'quote': lambda t:
        set_pass(False)+set_quote(True) + \
        '\n'+text(t)+inter(t)+'\n'+tail(t)
        if get_setting('回复')
        else set_quote(True)+tail(t),  # 引用
    'tip tip_4': lambda t: tail(t),  # 图像注释
    'tip tip_4 aimg_tip': lambda t: tail(t),  # 图像注释
    'modact': lambda t: tail(t),  # 状态
    'cm': lambda t: tail(t),  # 评论
    'rate': lambda t: tail(t),  # 评分
    'locked': lambda t: tail(t)  # 被删除的帖子
}


p_class = {
    'mbn': lambda t: tail(t)  # 图像注释
}

h3_class = {
    'psth xs1': lambda t: tail(t)  # 评分
}

dl_class = {
    'rate': lambda t:
        tail(t)
}

tag_name = {  # 根据标签名字判断函数
    'div': lambda t:
        div_class[t.get('class')](t)
        if t.get('class') in div_class
        else '\n'+text(t)+inter(t)+'\n'+tail(t),
    'p': lambda t:
        p_class[t.get('class')](t)
        if t.get('class') in p_class
        else '\n'+text(t)+inter(t)+'\n'+tail(t),
    'br': lambda t:
        '\n'+text(t)+inter(t)+tail(t),
    'i': lambda t:
        i_class[t.get('class')](t)
        if t.get('class') in i_class
        else text(t)+inter(t)+tail(t),
    'img': lambda t:
        img(t)
        if get_setting('图片') and t.get('file')
        else tail(t),
    'a': lambda t:
        '[https://lgqm.gq/'+t.get('href')+' '+text(t)+']'+tail(t)
        if 'https://' not in t.get('href')
        else '['+t.get('href')+' '+text(t)+']'+tail(t)
        if get_setting('链接') and t.get('href')
        else tail(t),
    'h2': lambda t:
        '\n'+get_setting('标题样式')+text(t) + \
        inter(t)+get_setting('标题样式')+'\n'+tail(t)
        if get_setting('自动标题') == 3
        else '\n'+text(t) + \
        inter(t)+'\n'+tail(t),
    'h3': lambda t:
        h3_class[t.get('class')](t)
        if t.get('class') in h3_class
        else '\n'+text(t)+inter(t)+'\n'+tail(t),
    'blockquote': lambda t:
        set_pass(True) + \
        after(t.find("br"))+'\n----\n'
        if not Status.is_pass
        else '\n<blockquote>\n'+text(t)+inter(t)+tail(t)+'\n</blockquote>\n',
    'li': lambda t:
        f'\n# '+text(t)+inter(t)+tail(t),
    'ul': lambda t:
        f'\n* '+text(t)+inter(t)+tail(t),
    'dl': lambda t:
        dl_class[t.get('class')](t)
        if t.get('class') in dl_class
        else '\n'+text(t)+inter(t)+'\n'+tail(t),  # 评分，也可以用 class='rate' 来跳过
    'strong': lambda t:
        "\n'''"+text(t)+inter(t)+"'''"+tail(t),
    'font': lambda t:
        f'<font color="{t.get("color")}">'+text(t)+inter(t)+'</font>'+tail(t)
        if get_setting('字体颜色') and t.get('color')
        else text(t)+inter(t)+tail(t)
}


def inter(t):  # 标签内部的内容
    content = str()
    for i in t.iterchildren():
        content += tag(i)
    return content


def tag(t):  # 仅作判断使用，与 inter() 相互调用
    return tag_name[t.tag](t) if t.tag in tag_name else text(t)+inter(t)+tail(t)
