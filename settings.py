import os
import json

settings = dict()


def set_setting(setting, value):
    global settings
    settings[setting] = value


def get_setting(setting=None):
    return settings[setting] if setting else settings


def load_settings():  # 加载设置
    global settings
    if os.path.exists('settings.json'):
        with open('settings.json', 'r', encoding='utf-8_sig') as f:
            settings = json.load(f)
    else:
        with open('default.json', 'r', encoding='utf-8_sig') as f:
            settings = json.load(f)


def log_func(value):
    print(value, end='')


def set_log(func):
    global log_func
    log_func = func


def log(value):
    return log_func(value)


def print_settings():
    for i, j in get_setting().items():
        log(f'{i}: {j}')


load_settings()
