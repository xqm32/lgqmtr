import re
import os
from tag import *
from settings import *


def auto_title(content):  # 以「第」字开头可以避免和首行标题的重复
    return \
        re.compile(r"^((''')?第.{1,10}[章节].*)", re.M).sub(r'{0}\1{0}'.format(get_setting('标题样式')), content) \
        if get_setting('自动标题') == 1 and len(content) >= get_setting('自动阙值') \
        else content


def floor(fl):
    Status.floor += 1
    Status.is_quote = False
    content = tag(fl)
    if (not get_setting('回复') and Status.is_quote
            or get_setting('限制字数') and len(content) < get_setting('限制阙值')):
        log(f'^')
        return str()
    log(f'*' if Status.is_quote else f'.')
    if get_setting('自动标题') == 2 and len(content) >= get_setting('自动阙值'):
        log(f'=')
        temp = content.split('\n')
        content = str()
        for i in range(0, len(temp)):
            if temp[i].strip() != str():
                temp[i] = f'{get_setting("标题样式")}{temp[i]}{get_setting("标题样式")}'
                break
        for i in temp:
            content += i+'\n'
    elif get_setting('自动标题') == 4 and len(content) >= get_setting('自动阙值'):
        log(f'=')
        content = f'\n{get_setting("标题样式")}{Status.floor}{get_setting("标题样式")}\n'+content
    elif get_setting('自动标题') == 5 and len(content) >= get_setting('自动阙值'):
        log(f'=')
        if Status.floor-1 != 0:
            content = f'\n{get_setting("标题样式")}{Status.floor-1}{get_setting("标题样式")}\n'+content
    if Status.is_quote:
        content = '\n{{同人注释start}}\n'+content+'\n{{同人注释end}}\n'
    if get_setting('分割线'):
        content += '\n----\n'
    return content


def page(p):
    log(f'正在处理源码：\n')
    content = str()
    for i in p.xpath('//div[@class="pcb"]'):
        content += floor(i)
    log(f' √\n')
    return content


def post(url, fr=1, to=1, fn=None):
    Status.floor = 0
    content = str()
    fn = fn if fn else get_tid(url)
    if not os.path.exists('output'):
        os.mkdir('output')
    for n in range(fr, to+1):
        content += page(get(generate_url(url, n)))
    content = re.compile(r'(\n|\r\n)+').sub('\n\n', content)
    content = auto_title(content)
    log(f'正在写入文本至：output/{fn}.mw')
    with open(f'output/{fn}.mw', 'w', encoding='utf-8_sig') as f:
        f.write(content)
    log(f' √\n')
    return


if __name__ == '__main__':
    arg = input().split()
    if len(arg) == 1:
        post(arg[0])
    elif len(arg) == 2:
        post(arg[0], int(arg[1]))
    elif len(arg) == 3:
        post(arg[0], int(arg[1]), int(arg[2]))
    elif len(arg) == 4:
        post(arg[0], int(arg[1]), int(arg[2]), arg[3])
